﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace medic.db
{
    // Reflection para obtener metadata del mapeo con la Base de datos.
    public class PropiedadAttribute : Attribute
    {
        private Type _tipo;
        public PropiedadAttribute()
        {
            this.EsClave = false;
            this.EsNullable = false;
            this.EsAutoGenerado = false;
            this.Longitud = 0;
        }
        public string Name { get; set; }
        public Type Tipo { get { return _tipo; } 
            set {
                if (value == typeof(DateTime))
                    this.Format = "yyyy-m-d hh:MM:ss";
                _tipo = value; } }
        public bool EsClave { get; set; }
        public bool EsAutoGenerado { get; set; }
        public int Longitud { get; set; }
        public bool EsNullable { get; set; }
        public string Format { get; set; } 
        public override string ToString()
        {
            return this.Name;
        }
    }

    public class TableAttribute : Attribute
    {
        public string Name { get; set; }
        public Type Tipo { get; set; }
        public override string ToString()
        {
            return this.Name;
        }
    }
}
