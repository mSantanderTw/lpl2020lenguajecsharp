﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace medic.db
{
    [Table(Name = "especialidad")]
    public partial class Especialidad
    {
        #region variables locales
        private int _id;
		private string _descripcion;
        #endregion        

        #region propiedades publicas
        
        [Propiedad(Name="id", Tipo=typeof(int), EsAutoGenerado=true, EsClave=true)]
		public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [Propiedad(Name = "descripcion", Tipo=typeof(string), Longitud=50)]
		public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }
        #endregion        
    }
    
}
