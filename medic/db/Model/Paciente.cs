﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace medic.db
{
    [Table(Name = "paciente")]
    public partial class Paciente
    {
        #region variables locales
        private int _id;
        private int _nro_legajo;
        private DateTime _fecha_nac;
        private int _dni;
        private string _apellido;
        private string _nombres;
        private string _domicilio;
        private string _telefono;
        private int _cod_obra_social;
        private int _cod_localidad;
        #endregion

        #region propiedades publicas

        [Propiedad(Name = "id", Tipo = typeof(int), EsAutoGenerado = true, EsClave = true)]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [Propiedad(Name = "nro_legajo", Tipo = typeof(int))]
        public int NroLegajo
        {
            get { return _nro_legajo; }
            set { _nro_legajo = value; }
        }

        [Propiedad(Name = "fecha_nac", Tipo = typeof(DateTime))]
        public DateTime FechaNac
        {
            get { return _fecha_nac; }
            set { _fecha_nac = value; }
        }

        [Propiedad(Name = "dni", Tipo = typeof(int))]
        public int Dni
        {
            get { return _dni; }
            set { _dni = value; }
        }

        [Propiedad(Name = "apellido", Tipo = typeof(string), Longitud = 50)]
        public string Apellido
        {
            get { return _apellido; }
            set { _apellido = value; }
        }

        [Propiedad(Name = "nombres", Tipo = typeof(string), Longitud = 90)]
        public string Nombres
        {
            get { return _nombres; }
            set { _nombres = value; }
        }

        [Propiedad(Name = "domicilio", Tipo = typeof(string), Longitud = 20)]
        public string Domicilio
        {
            get { return _domicilio; }
            set { _domicilio= value; }
        }

        [Propiedad(Name = "telefono", Tipo = typeof(string), Longitud = 20)]        
        public string Telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }

        [Propiedad(Name = "cod_obra_social", Tipo = typeof(int))]
        public int CodObraSocial
        {
            get { return _cod_obra_social; }
            set { _cod_obra_social = value; }
        }

        [Propiedad(Name = "cod_localidad", Tipo = typeof(int))]
        public int CodLocalidad
        {
            get { return _cod_localidad; }
            set { _cod_localidad = value; }
        }
        #endregion  
    }
}
