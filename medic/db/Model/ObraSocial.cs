﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace medic.db
{
    [Table(Name = "obra_social")]
    public partial class ObraSocial
    {
        #region variables locales
        private int _id;
        private string _descripcion;
        private string _domicilio_principal;
        #endregion

        #region propiedades publicas

        [Propiedad(Name = "id", Tipo = typeof(int), EsAutoGenerado = true, EsClave = true)]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [Propiedad(Name = "descripcion", Tipo = typeof(string), Longitud = 50)]
        public string Descripcion
        {
            get { return _descripcion; }
            set { _descripcion = value; }
        }

        [Propiedad(Name = "domicilio_principal", Tipo = typeof(string), Longitud = 90)]
        public string DomicilioPrincipal
        {
            get { return _domicilio_principal; }
            set { _domicilio_principal = value; }
        }
        #endregion
    }

}
