﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace medic.db
{
    [Table(Name = "consultorio")]
    public partial class Consultorio
    {
        #region variables locales
        private int _id;
        private string _denominacion;
        private string _domicilio;
        private string _telefono;
        #endregion

        #region propiedades publicas

        [Propiedad(Name = "id", Tipo = typeof(int), EsAutoGenerado = true, EsClave = true)]
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        [Propiedad(Name = "denominacion", Tipo = typeof(string), Longitud = 40)]
        public string Denominacion
        {
            get { return _denominacion; }
            set { _denominacion = value; }
        }

        [Propiedad(Name = "domicilio", Tipo = typeof(string), Longitud = 90)]
        public string Domicilio
        {
            get { return _domicilio; }
            set { _domicilio = value; }
        }

        [Propiedad(Name = "telefono", Tipo = typeof(string), Longitud = 90)]
        public string Telefono
        {
            get { return _telefono; }
            set { _telefono = value; }
        }
        #endregion  
    }
}
