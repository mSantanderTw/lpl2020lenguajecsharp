﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using medic.db.orm;

namespace medic.db
{
    public partial class Consultorio : BaseClass, IAccessDB<Consultorio>
    {  // HACER VERIFICACIONES ANTES DE REALIZAR OPERACIONES DE ACTUALIZACION, LANZAR EVENTO O EXCEPCION
        public event ValidarClaseDelegate ValidacionConsultorioGuardar;
        public List<Consultorio> FindAll()
        {
            return this.FindAll(null);
        }
        public List<Consultorio> FindAll(string criterio)
        {
            return ORMDB<Consultorio>.FindAll(criterio);
        }
        public Consultorio FindbyKey(params object[] key)
        {
            var cons = ORMDB<Consultorio>.FindbyKey(key);
            // completar datos en this para dejarlo inicializado
            this.Id = cons.Id;
            this.Denominacion = cons.Denominacion;
            this.Domicilio = cons.Domicilio;
            this.Telefono = cons.Telefono;
            this.SetIsObjFromDB();
            return this;
        }
        public bool SaveObj()
        {
            if (!this.IsNew)
            {
                if (this.ValidacionConsultorioGuardar != null)
                {
                    if (Denominacion == "")
                        ValidacionConsultorioGuardar("No se puede poner Denominacion vacia");
                }
            }
            return ORMDB<Consultorio>.SaveObject(this);
        }
        // Metodos estaticos para no usar una instancia para acceder a metodo FindAll-Consultorio
        public static List<Consultorio> FindAllStatic(string criterio, Comparison<Consultorio> compara)
        {
            var lista = ORMDB<Consultorio>.FindAll(criterio);
            lista.Sort(compara);
            return lista;
        }
    }
}
