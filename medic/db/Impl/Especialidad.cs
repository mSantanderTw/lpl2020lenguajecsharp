﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using medic.db.orm;

namespace medic.db
{
    public partial class Especialidad : BaseClass, IAccessDB<Especialidad>
    {  // HACER VERIFICACIONES ANTES DE REALIZAR OPERACIONES DE ACTUALIZACION, LANZAR EVENTO O EXCEPCION
        public event ValidarClaseDelegate ValidacionEspecialidadGuardar;
        public List<Especialidad> FindAll()
        {
            return this.FindAll(null);
        }
        public List<Especialidad> FindAll(string criterio)
        {
            return ORMDB<Especialidad>.FindAll(criterio); 
        }
        public Especialidad FindbyKey(params object[] key)
        {
            var esp = ORMDB<Especialidad>.FindbyKey(key);
            // completar datos en this para dejarlo inicializado
            this.Id = esp.Id;
            this.Descripcion = esp.Descripcion;
            this.SetIsObjFromDB();
            return this;
        }
        public bool SaveObj()
        {
            if (!this.IsNew)
            {
                if (this.ValidacionEspecialidadGuardar != null)
                {
                    if(Descripcion == "" )
                        ValidacionEspecialidadGuardar("No se puede poner Descripcion vacia");
                }
            }
            return ORMDB<Especialidad>.SaveObject(this);
        }
        // Metodos estaticos para no usar una instancia para acceder a metodo FindAll-Especialidad
        public static List<Especialidad> FindAllStatic(string criterio, Comparison<Especialidad> compara )
        {
            var lista = ORMDB<Especialidad>.FindAll(criterio);
            lista.Sort(compara);
            return lista;
        }
    }
}
