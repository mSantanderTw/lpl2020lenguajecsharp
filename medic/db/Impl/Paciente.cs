﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using medic.db.orm;

namespace medic.db
{
    public partial class Paciente : BaseClass, IAccessDB<Paciente>
    {  // HACER VERIFICACIONES ANTES DE REALIZAR OPERACIONES DE ACTUALIZACION, LANZAR EVENTO O EXCEPCION
        public event ValidarClaseDelegate ValidacionPacienteGuardar;
        public List<Paciente> FindAll()
        {
            return this.FindAll(null);
        }
        public List<Paciente> FindAll(string criterio)
        {
            return ORMDB<Paciente>.FindAll(criterio);
        }
        public Paciente FindbyKey(params object[] key)
        {
            var pac = ORMDB<Paciente>.FindbyKey(key);
            // completar datos en this para dejarlo inicializado
            this.Id = pac.Id;
            this.NroLegajo = pac.NroLegajo;
            this.FechaNac = pac.FechaNac;
            this.Dni = pac.Dni;
            this.Apellido = pac.Apellido;
            this.Nombres = pac.Nombres;
            this.Domicilio = pac.Domicilio;
            this.Telefono = pac.Telefono;
            this.CodLocalidad = pac.CodLocalidad;
            this.CodObraSocial = pac.CodObraSocial;
            this.SetIsObjFromDB();
            return this;
        }
        public bool SaveObj()
        {
            if (!this.IsNew)
            {
                if (this.ValidacionPacienteGuardar != null)
                {
                    if (Dni == 0)
                        ValidacionPacienteGuardar("No se puede poner Dni cero");
                }
            }
            return ORMDB<Paciente>.SaveObject(this);
        }
        // Metodos estaticos para no usar una instancia para acceder a metodo FindAll-Paciente
        public static List<Paciente> FindAllStatic(string criterio, Comparison<Paciente> compara)
        {
            var lista = ORMDB<Paciente>.FindAll(criterio);
            lista.Sort(compara);
            return lista;
        }
    }
}
