﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using medic.db.orm;

namespace medic.db
{
    public partial class ObraSocial : BaseClass, IAccessDB<ObraSocial>
    {  // HACER VERIFICACIONES ANTES DE REALIZAR OPERACIONES DE ACTUALIZACION, LANZAR EVENTO O EXCEPCION
        public event ValidarClaseDelegate ValidacionObraSocialGuardar;
        public List<ObraSocial> FindAll()
        {
            return this.FindAll(null);
        }
        public List<ObraSocial> FindAll(string criterio)
        {
            return ORMDB<ObraSocial>.FindAll(criterio); 
        }
        public ObraSocial FindbyKey(params object[] key)
        {
            var loc = ORMDB<ObraSocial>.FindbyKey(key);
            // completar datos en this para dejarlo inicializado
            this.Id = loc.Id;
            this.Descripcion = loc.Descripcion;
            this.DomicilioPrincipal = loc.DomicilioPrincipal;
            this.SetIsObjFromDB();
            return this;
        }
        public bool SaveObj()
        {
            if (!this.IsNew)
            {
                if (this.ValidacionObraSocialGuardar != null)
                {
                    if(Descripcion == "" )
                        ValidacionObraSocialGuardar("No se puede poner Nombre vacio");
                }
            }
            return ORMDB<ObraSocial>.SaveObject(this);
        }
        // Metodos estaticos para no usar una instancia para acceder a metodo FindAll-ObraSocial
        public static List<ObraSocial> FindAllStatic(string criterio, Comparison<ObraSocial> compara)
        {
            var lista = ORMDB<ObraSocial>.FindAll(criterio);
            lista.Sort(compara);
            return lista;
        }
    }
}
