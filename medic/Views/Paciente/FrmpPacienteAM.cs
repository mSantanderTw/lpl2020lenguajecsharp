﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using medic.db;

namespace medic.Views
{
    public delegate void DelegatePacienteCargado(Paciente paciente);
    public partial class FrmpPacienteAM : FormBase
    {
        public event DelegatePacienteCargado PacienteCargado;
        public FrmpPacienteAM()
        {
            InitializeComponent();
        }

        private void FrmpPacienteAM_Load(object sender, EventArgs e)
        {
            
        }

        private void CancelarBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GuardarBtn_Click(object sender, EventArgs e)
        {
            Paciente paciente = null;
            // ver si el paciente
            if (PacienteCargado != null)
                PacienteCargado(paciente);
            this.Close();

        }
    }
}
