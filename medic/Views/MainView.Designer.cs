﻿namespace medic.Views
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.EspecialidadMnu = new System.Windows.Forms.ToolStripMenuItem();
            this.IngresoEspecialidadMnu = new System.Windows.Forms.ToolStripMenuItem();
            this.ListadoEspecialidadMnu = new System.Windows.Forms.ToolStripMenuItem();
            this.buscarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EspecialidadMnu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(967, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // EspecialidadMnu
            // 
            this.EspecialidadMnu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IngresoEspecialidadMnu,
            this.ListadoEspecialidadMnu,
            this.buscarToolStripMenuItem});
            this.EspecialidadMnu.Name = "EspecialidadMnu";
            this.EspecialidadMnu.Size = new System.Drawing.Size(105, 24);
            this.EspecialidadMnu.Text = "Especialidad";
            // 
            // IngresoEspecialidadMnu
            // 
            this.IngresoEspecialidadMnu.Name = "IngresoEspecialidadMnu";
            this.IngresoEspecialidadMnu.Size = new System.Drawing.Size(152, 24);
            this.IngresoEspecialidadMnu.Text = "Ingreso";
            this.IngresoEspecialidadMnu.Click += new System.EventHandler(this.IngresoEspecialidadMnu_Click);
            // 
            // ListadoEspecialidadMnu
            // 
            this.ListadoEspecialidadMnu.Name = "ListadoEspecialidadMnu";
            this.ListadoEspecialidadMnu.Size = new System.Drawing.Size(152, 24);
            this.ListadoEspecialidadMnu.Text = "Listado";
            // 
            // buscarToolStripMenuItem
            // 
            this.buscarToolStripMenuItem.Name = "buscarToolStripMenuItem";
            this.buscarToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.buscarToolStripMenuItem.Text = "Buscar...";
            this.buscarToolStripMenuItem.Click += new System.EventHandler(this.buscarToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 525);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(967, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 547);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "MainView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MedicApp: Registro de información";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainView_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem EspecialidadMnu;
        private System.Windows.Forms.ToolStripMenuItem IngresoEspecialidadMnu;
        private System.Windows.Forms.ToolStripMenuItem ListadoEspecialidadMnu;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripMenuItem buscarToolStripMenuItem;

    }
}