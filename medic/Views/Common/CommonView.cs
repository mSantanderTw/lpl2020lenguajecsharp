﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace medic.Views
{
    public enum FrmOperacion
    {
        frmAlta=1,
        frmModificacion=2,
        frmConsulta=3
    };
    public enum TipoOperacionStatus
    {
        stOK =1,
        stError = 2
    }
    public class EventArgDom : EventArgs
    {
        public TipoOperacionStatus Status { get; set; }
        public string Mensaje { get; set; }
        public Object ObjProcess { get; set; }
    }

    // Delegado Generico para poder mandar Mensaje Generico a formulario invocador del form.
    public delegate void FormEvent(object Sender, EventArgDom ev);

    public class FormBase: Form
    {
        public virtual event FormEvent DoCompleteOperationForm = delegate { };
        public virtual FrmOperacion OperacionForm { get; set; }
        public virtual void Reload(){}
        public FormBase InvokerForm;
    }
}
