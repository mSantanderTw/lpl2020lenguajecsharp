﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using medic.db;

namespace medic.Views
{
    public partial class FrmEspecialidadList : FormBase
    {
        private string _criterio = null;
        private List<Especialidad> _listado;
        public FrmEspecialidadList()
        {
            InitializeComponent();
        }

        public void ShowListado(List<Especialidad> listado, FormBase Invoker, string criterio)
        {
            this.InvokerForm = Invoker;
            _listado = listado;
            _criterio = criterio;
            this.EspecialidadesGrd.AutoGenerateColumns = false;
            this.EspecialidadesGrd.DataSource = listado;
            InvokerForm.Close();
            this.MdiParent = MainView.Instance;
            this.Show();

        }
        private void CerrarBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmEspecialidadList_Load(object sender, EventArgs e)
        {
            this.EspecialidadesGrd.AutoGenerateColumns = false;
        }

        private void EspecialidadesGrd_DoubleClick(object sender, EventArgs e)
        {
            if (this.EspecialidadesGrd.SelectedRows.Count > 0)
            {
                MainView.Instance.Cursor = Cursors.WaitCursor;   
                FrmEspecialidadAM frm = new FrmEspecialidadAM();
                frm.DoCompleteOperationForm += new FormEvent(frm_DoCompleteOperationForm);
                frm.ShowModificarEspecialidad(this,(this.EspecialidadesGrd.SelectedRows[0].DataBoundItem as Especialidad));
            }
        }

        void frm_DoCompleteOperationForm(object Sender, EventArgDom ev)
        {
            this.Cursor = Cursors.Default;
            if (ev.Status == TipoOperacionStatus.stOK)
            {
                var selAnt = EspecialidadesGrd.SelectedRows[0].Index;
                this.EspecialidadesGrd.DataSource = Especialidad.FindAllStatic(_criterio, (e1,e2)=>e1.Id.CompareTo(e2.Id));                
                EspecialidadesGrd.Rows[selAnt].Selected = true;
                MessageBox.Show("Especialidad actualizada", "Exito...", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }        
    }
}
