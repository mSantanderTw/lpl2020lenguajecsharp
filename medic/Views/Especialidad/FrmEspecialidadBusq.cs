﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using medic.db;

namespace medic.Views
{
    public partial class FrmEspecialidadBusq : FormBase
    {
        public FrmEspecialidadBusq()
        {
            InitializeComponent();
        }

        private void DescripcionChk_CheckStateChanged(object sender, EventArgs e)
        {
            this.DescripcionTxt.Enabled = this.DescripcionChk.Checked;
        }

        private void CancelarBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void ShowBuscar()
        {
            this.Show();
        }

        private void BuscarBtn_Click(object sender, EventArgs e)
        {
            MainView.Instance.Cursor = Cursors.WaitCursor;
            // verificar si hay multiples opciones a usar como filtro que elija alguna, si son dos campos, no hace falta.            
            string criterio = null;
            if (this.DescripcionChk.Checked)
            {
                criterio = String.Format("descripcion like '%{0}%'", DescripcionTxt.Text);
            }
            try
            {
                var lista = Especialidad.FindAllStatic(criterio, (e1, e2) => e1.Id.CompareTo(e2.Id));
                MainView.Instance.Cursor = Cursors.Default;
                
                if (lista.Count == 0)
                {
                    MessageBox.Show("No se encontraron resultados con criterio ingresado", "Sin resultados...", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                // invocar Formulario de Listado.
                FrmEspecialidadList frm = new FrmEspecialidadList();
                frm.ShowListado(lista,this, criterio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error: " + ex.Message, "Error...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void FrmEspecialidadBusq_Activated(object sender, EventArgs e)
        {
            MainView.Instance.Cursor = Cursors.Default;
        }
    }
}
