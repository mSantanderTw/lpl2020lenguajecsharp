﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using medic.db;
namespace medic.Views
{    
    public partial class FrmEspecialidadAM : FormBase
    {
        public override event FormEvent DoCompleteOperationForm;

        private Especialidad _especialidad_modif = null;
        public FrmEspecialidadAM()
        {
            InitializeComponent();
        }

        private void FrmEspecialidadInfo_Load(object sender, EventArgs e)
        {

        }

        public override FrmOperacion OperacionForm
        {
            get
            {
                return base.OperacionForm;
            }
            set
            {
                base.OperacionForm = value;
                if (value == FrmOperacion.frmAlta)
                {
                    this.Text = "Ingreso de nueva especialidad...";
                }
                if (value == FrmOperacion.frmModificacion)
                {
                    this.Text = "Actualizacion de datos de especialidad...";
                }
                if (value == FrmOperacion.frmConsulta)
                {
                    this.Text = "Consulta de datos de especialidad...";
                    this.GuardarBtn.Visible = false;
                }
            }
        }

        
        private void GuardarBtn_Click(object sender, EventArgs e)
        {
            Especialidad especialidad = null;
            string errMsj = "";
            MainView.Instance.Cursor = Cursors.WaitCursor;
            if (OperacionForm == FrmOperacion.frmAlta)
            {
                especialidad = new Especialidad();
                // cargar la info de la especialidad antes de dar de alta.
            }
            if (OperacionForm == FrmOperacion.frmModificacion)
            {
                especialidad = _especialidad_modif;
            }
            // SET CAMPOS DE LOS CONTROLES A LOS ATRIBUTOS
            especialidad.Descripcion = this.DescripcionTxt.Text;
            // intentar guardar en la Base de datos.
            try
            {
                especialidad.SaveObj();
            }
            catch (Exception ex)
            {
                errMsj = "Error: " + ex.Message;
            }
            // si esta configurado, al form invoker enviarle evento de operacion completa
            if (DoCompleteOperationForm != null)
            {
                if (errMsj  == "" )
                    DoCompleteOperationForm(especialidad, new EventArgDom {  ObjProcess= especialidad, Status= TipoOperacionStatus.stOK }); 
                else
                    DoCompleteOperationForm(especialidad, new EventArgDom { ObjProcess = especialidad, Mensaje =errMsj, Status = TipoOperacionStatus.stError}); 
            }

            if (this.InvokerForm != null)
            {
                InvokerForm.Reload();
            }
            MainView.Instance.Cursor = Cursors.Default;
            this.Close();
        }
        
        public void ShowModificarEspecialidad(FormBase Invoker, Especialidad Esp_modif)
        {
            this.OperacionForm = FrmOperacion.frmModificacion;
            _especialidad_modif = Esp_modif;
            this.IdTxt.Text = _especialidad_modif.Id.ToString();
            this.DescripcionTxt.Text = _especialidad_modif.Descripcion;
            this.InvokerForm = Invoker;
            this.ShowDialog();
        }

        public void ShowModificarEspecialidad(Especialidad Esp_modif)
        {
            this.OperacionForm = FrmOperacion.frmModificacion;
            _especialidad_modif = Esp_modif;
            this.InvokerForm = null;
            this.ShowDialog();
        }
        public void ShowIngresoEspecialidad( FormBase Invoker )
        {
            this.InvokerForm = Invoker;
            this.OperacionForm = FrmOperacion.frmAlta;
            this.ShowDialog();
        }
        public void ShowIngresoEspecialidad()
        {
            this.InvokerForm = null;
            this.OperacionForm = FrmOperacion.frmAlta;
            this.ShowDialog();
        }

        private void CancelarBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmEspecialidadAM_Activated(object sender, EventArgs e)
        {
            MainView.Instance.Cursor = Cursors.Default;   
        }
    }
}
